package cadastro.mobile.utfpr.edu.br.cadastro;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ConfirmarActivity extends AppCompatActivity {

    private TextView tvCod;
    private TextView tvQtd;
    private TextView tvValor;
    private Button btConfirmar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmar);

        tvCod = (TextView) findViewById( R.id.tvCodLancamento );
        tvQtd = (TextView) findViewById( R.id.tvQtdLancamento );
        tvValor = (TextView) findViewById( R.id.tvValorLancamento );
        btConfirmar = (Button) findViewById( R.id.btConfirmarFinal );

        Intent i = getIntent();

        int cod = i.getIntExtra("cod", 0);
        double qtd = i.getDoubleExtra("qtd", 0.00);
        double valor = i.getDoubleExtra("valor", 0);

        tvCod.setText( String.valueOf( cod ) );
        tvQtd.setText( String.valueOf( qtd ) );
        tvValor.setText( String.valueOf( valor ) );

    }

    public void btConfirmarOnClick(View view) {

        //Toast.makeText(this, "Produto confirmado", Toast.LENGTH_LONG).show();
        AlertDialog.Builder alerta = new AlertDialog.Builder( this );

        alerta.setTitle( "ERRO" );
        alerta.setMessage( "Erro no acesso à rede." );
        alerta.setNeutralButton( "OK", null );
        alerta.setCancelable( false );
        alerta.show();

        //  finish();

    }
}
