package cadastro.mobile.utfpr.edu.br.cadastro;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class LancamentoActivity extends AppCompatActivity {

    private static final int ID_COMANDO_LISTAR = 0;

    private EditText etCod;
    private EditText etQtd;
    private EditText etValor;
    private Button btConfirmar;
    private Button btListar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lancamento);

        etCod = (EditText) findViewById( R.id.etCodLancamento );
        etQtd = (EditText) findViewById( R.id.etQtdLancamento );
        etValor = (EditText) findViewById( R.id.etValorLancamento );
        btConfirmar = (Button) findViewById( R.id.btConfirmarLancamento );
        btListar = (Button) findViewById( R.id.btListarLancamento );

    }

    public void btConfirmarOnClick(View view) {

        Intent i = new Intent( this, ConfirmarActivity.class );

        i.putExtra( "cod", Integer.parseInt( etCod.getText().toString() ) );
        i.putExtra( "qtd", Double.parseDouble( etQtd.getText().toString() ) );
        i.putExtra( "valor", Double.parseDouble( etValor.getText().toString() ) );

        startActivity( i );


    }

    public void btListarOnClick(View view) {

        Intent i = new Intent( this, ListarActivity.class );
        startActivityForResult( i, ID_COMANDO_LISTAR);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if ( requestCode == ID_COMANDO_LISTAR ) {
            if ( data != null ) {
                int cod = data.getIntExtra( "cod", 0 );
                etCod.setText( String.valueOf( cod ) );
            }
        }

    }
}
