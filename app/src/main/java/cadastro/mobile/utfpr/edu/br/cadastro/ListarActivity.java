package cadastro.mobile.utfpr.edu.br.cadastro;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class ListarActivity extends AppCompatActivity {

    private static final int CODIGO_RETORNO = 0;

    private ListView lvProdutos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar);

        lvProdutos = (ListView) findViewById( R.id.lvProdutos );

        lvProdutos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tratarEventoClique( position );
            }
        });

    }

    private void tratarEventoClique(int position) {
        int codigo = ++position;

        Intent i = getIntent();
        i.putExtra( "cod" , codigo);

        setResult(CODIGO_RETORNO, i );

        finish();

    }
}
